#include "gate.h"
void gate::init(double t,...) {
	//The 'parameters' variable contains the parameters transferred from the editor.
	printLog("Gate init \n");
	va_list parameters;
	va_start(parameters,t);
	//To get a parameter: %Name% = va_arg(parameters,%Type%)
	//where:
	//      %Name% is the parameter name
	//	%Type% is the parameter type
	char *fvar= va_arg(parameters,char*);

	kg1=getScilabVar(fvar);
	fvar= va_arg(parameters,char*);
	kg2=getScilabVar(fvar);
	fvar= va_arg(parameters,char*);
	kg3=getScilabVar(fvar);
	fvar= va_arg(parameters,char*);

	seedGenerator0Kg1 = getScilabVar(fvar);
	fvar= va_arg(parameters,char*);
	seedGeneratorKg2Kg3 = getScilabVar(fvar);
	fvar= va_arg(parameters,char*);
	seedGenerator0Kg1e = getScilabVar(fvar);
	fvar= va_arg(parameters,char*);

	generator0Kg1e = new uniformDist(0.5,kg1,seedGenerator0Kg1e);
	generator0Kg1 = new uniformDist(0.5,kg1,seedGenerator0Kg1);
	generatorKg2Kg3 = new uniformDist(kg2,kg3,seedGeneratorKg2Kg3);

	s = GATE_STATE_OPEN;
	sigma = INF;
	y = 0;
	tempState = 0;
	tempTime = 0;
}
double gate::ta(double t) {
//This function returns a double.
printLog("Gate Time advance \n");
return sigma;
}
void gate::dint(double t) {
printLog("Gate internal transition \n");
if (s == GATE_STATE_LOWERING){ //Lowering
	sigma = INF;
	s = GATE_STATE_CLOSE; //C=Close
} else if (s == GATE_STATE_RAISING){ //R=Raising
	sigma = INF;
	s = GATE_STATE_OPEN; //O=Open
} else if (s == GATE_STATE_CHANGING){
	s = tempState;
	sigma = tempTime;
}
}
void gate::dext(Event x, double t) {
	printLog("Gate external transition \n");
	//The input event is in the 'x' variable.
	//where:
	//     'x.value' is the value (pointer to void)
	//     'x.port' is the port number
	//     'e' is the time elapsed since last transition
	double *xv = (double*)(x.value);

	if (s==GATE_STATE_OPEN && *xv == MEDIATOR_LOWER){ //L=Lower
		tempState = GATE_STATE_LOWERING; //Lowering
		tempTime = generator0Kg1->getNumber();//uniforme (0,kg1)
		s = GATE_STATE_CHANGING;
		sigma = 0;
	}else if (s == GATE_STATE_CLOSE && *xv == MEDIATOR_RAISE) { //R=Raise
		s = GATE_STATE_CHANGING;
		sigma = 0;
		tempState = GATE_STATE_RAISING; //R=Raising
		tempTime = generatorKg2Kg3->getNumber(); //uniforme (kg2, kg3)
	}else if (s == GATE_STATE_RAISING && *xv == MEDIATOR_LOWER){ //R=Raising ,L=Lower
		s = GATE_STATE_LOWERING; //L=Lowering 
		generator0Kg1e->setBounds(0.5,(kg1-e));
		sigma = generator0Kg1e->getNumber();
	}else{
		printLog("********************ERROR Gate: caso no soportado\n");
	}
}
Event gate::lambda(double t) {
	//This function returns an Event:
	//     Event(%&Value%, %NroPort%)
	//where:
	//     %&Value% points to the variable which contains the value.
	//     %NroPort% is the port number (from 0 to n-1)
	printLog("Gate  output \n");
	switch(s){
		case GATE_STATE_LOWERING: //Lowering
			y = GATE_DOWN; //down
		break;
		case GATE_STATE_CHANGING:
			y = GATE_CHANGING;
		break;
		case GATE_STATE_RAISING: //Raising
			y = GATE_UP; //up
		break;
	}
	return Event(&y,0);
}

void gate::exit() {
	//Code executed at the end of the simulation.
	printLog("Gate exit \n");
	delete generator0Kg1;
	delete generatorKg2Kg3;
	delete generator0Kg1e;
}
