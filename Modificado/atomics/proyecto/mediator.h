//CPP:proyecto/uniformDist.cpp
//CPP:proyecto/mediator.cpp
#if !defined mediator_h
#define mediator_h

#include "simulator.h"
#include "event.h"
#include "stdarg.h"

#include "proyecto/uniformDist.h"
#include "proyecto/const.h"

class mediator: public Simulator { 

// Declare the state,
double sigma;
int s;

// o+utput variables
double y;

// and parameters
double kc1;
double kc2;
unsigned int seedGenerator;

//uniforms
uniformDist* generator;

public:
	mediator(const char *n): Simulator(n) {};
	void init(double, ...);
	double ta(double t);
	void dint(double);
	void dext(Event , double );
	Event lambda(double);
	void exit();
};
#endif
