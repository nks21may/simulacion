
#if !defined CONST_H
#define CONST_H

//General constants
#define INF 1e20

//Train constants outputs
#define TRAIN_APPROACH -1
#define TRAIN_EXIT 1
#define TRAIN_IN 0

//Train constants states
#define TRAIN_STATE_FAR 2
#define TRAIN_STATE_NEAR 3
#define TRAIN_STATE_IN 4


//Controller constants uotputs
#define MEDIATOR_LOWER 1.2
#define MEDIATOR_RAISE 0.2

//Controller constans states
#define MEDIATOR_STATE_SC1 1
#define MEDIATOR_STATE_SC2 2
#define MEDIATOR_STATE_SC3 3
#define MEDIATOR_STATE_SC4 4

//Gate constans states
#define GATE_STATE_OPEN 1
#define GATE_STATE_LOWERING 2
#define GATE_STATE_CLOSE 3
#define GATE_STATE_RAISING 4
#define GATE_STATE_CHANGING 5

//Gate constans outputs
#define GATE_UP 1.2
#define GATE_DOWN 0.2
#define GATE_CHANGING 0.7

#endif