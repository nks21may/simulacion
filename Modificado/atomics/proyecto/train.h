//CPP:proyecto/uniformDist.cpp
//CPP:proyecto/train.cpp
#if !defined train_h
#define train_h

#include "simulator.h"
#include "event.h"
#include "stdarg.h"

#include "proyecto/uniformDist.h"
#include "proyecto/const.h"


class train: public Simulator { 

// State,
double sigma;
int s;

// output variables
int i;
double y;

// and parameters
double kt1;
double kt2;
unsigned int seedGenerator01;
unsigned int seedGeneratorKt1Kt2;
unsigned int seedGeneratorKt2;

//generadores
uniformDist* generator01;
uniformDist* generatorKt1Kt2;
uniformDist* generatorKt2;

public:
	train(const char *n): Simulator(n) {};
	void init(double, ...);
	double ta(double t);
	void dint(double);
	void dext(Event , double );
	Event lambda(double);
	void exit();
};
#endif
