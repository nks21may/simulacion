import java.io.*;
import java.util.*;

class StatisticalInformation{
	public final static double T_STUDENT  = 1.729; //t(n-1,alpha 0.05) n = 20 lotes

	public static ArrayList<String[]> readCvs(String path) throws IOException{
		BufferedReader csvReader = new BufferedReader(new FileReader(path));
		String row = "";
		ArrayList<String[]> data = new ArrayList<String[]>();
		while (csvReader.ready()) {
			row = csvReader.readLine();
		    data.add(row.split(", "));
		}
		csvReader.close();
		return data;
	}

	//Uso de la app
	public static void usage(){
		System.out.println("java StatisticalInformation 'tren.cvs' <nro trenes> ");
	}

	//Calcula el promedio de una lista
	public static Double promedio(List<Double> x){
		return x.stream().reduce(0.0, Double::sum)/x.size();
	}

	//Saca el tiempo entre un approach y un exit y devuelve una lista con esos tiempos (exit-approach)
	public static List<Double> clean(List<String[]> x, int y, int z){
		double a=0,n;
		int aux = 0;
		String[] s;
		List<Double> res = new ArrayList<Double>();
		for (int i = 0;i < x.size(); i++) {
			s = x.get(i);
			n = Double.parseDouble(s[0]);
			aux = Integer.parseInt(s[1]);
			if(aux == y){
				a = n;
			}
			if (aux == z){
				res.add(n-a);
			}
		}
		return res;
	}

	//Calcula la varianza
	public static Double varianza(List<Double> x){
		Double media = promedio(x);
		return x.stream().map((Double k) -> (Math.pow(k-media,2)/(x.size()-1))).reduce(0.0,Double::sum);
	}

	public static void main(String[] args) throws IOException{
		if (args.length < 2) {
			usage();
			throw new IllegalArgumentException();
		}

		//Lee  los trenes del archivo
		ArrayList<String[]> tren = readCvs(args[0]);

		//multiplica x2 para saber la cantidad de eventos a analizar
		int cantTren = Integer.parseInt(args[1])*2;

		ArrayList<Double> sal = new ArrayList<Double>();
		for (int i=0; i < tren.size() && i+cantTren < tren.size(); i+=cantTren) {
			sal.add(promedio(clean(tren.subList(i, i+cantTren), -1, 1)));
		}

		//output
		System.out.println("----------------");
		System.out.println("Con lotes de "+cantTren/2+" trenes hay "+sal.size()+" lotes");
		//System.out.println(sal);
		Double pro = promedio(sal);
		System.out.println("Media: "+pro);
		Double varianza = varianza(sal);
		System.out.println("Varianza: "+varianza);

		Double stu = T_STUDENT*Math.sqrt(varianza/sal.size());
		System.out.println("Intervalo ["+(pro-stu)+", "+(pro+stu)+"]");

	}

}
