#include "train.h"
void train::init(double t,...) {
	//The 'parameters' variable contains the parameters transferred from the editor.
	printLog("Train init \n");
	va_list parameters;
	va_start(parameters,t);
	//To get a parameter: %Name% = va_arg(parameters,%Type%)
	//where:
	//      %Name% is the parameter name
	//	%Type% is the parameter type
	char *fvar= va_arg(parameters,char*);

	kt1=getScilabVar(fvar);
	fvar= va_arg(parameters,char*);
	kt2=getScilabVar(fvar);
	fvar= va_arg(parameters,char*);

	seedGenerator01 = getScilabVar(fvar);
	fvar= va_arg(parameters,char*);
	seedGeneratorKt1Kt2 = getScilabVar(fvar);
	fvar= va_arg(parameters,char*);
	seedGeneratorKt2 = getScilabVar(fvar);
	fvar= va_arg(parameters,char*);

	generator01 = new uniformDist(0.1,1,seedGenerator01);
	generatorKt2 = new uniformDist(kt2-sigma,kt2,seedGeneratorKt2);
	generatorKt1Kt2 = new uniformDist(kt1,kt2,seedGeneratorKt1Kt2);

	sigma = generator01->getNumber();
	s = TRAIN_STATE_FAR;
	i = 0;
	y = 10;
}

double train::ta(double t) {
	//This function returns a double.
	printLog("Train time advance \n");
	return sigma;
}

void train::dint(double t) {
	printLog("Train internal transition \n");
	switch(s){
		case TRAIN_STATE_IN: s = TRAIN_STATE_FAR;
				printLog("From in to far \n");
				sigma = generator01->getNumber();		//uniforme entre (0,1)
		break;
		case TRAIN_STATE_FAR: s = TRAIN_STATE_NEAR;
				printLog("From far to near \n");
				sigma = generatorKt1Kt2->getNumber();    //uniforme entre (kt1, kt2)
		break;	
		case TRAIN_STATE_NEAR: s = TRAIN_STATE_IN;
				printLog("From near to in \n");
				generatorKt2->setBounds(kt2-sigma,kt2);
				sigma = generatorKt2->getNumber(); //uniforme entre (kt2-sigma,kt2)	
		break;
	}	
}

void train::dext(Event x, double t) {
	//The input event is in the 'x' variable.
	//where:
	//     'x.value' is the value (pointer to void)
	//     'x.port' is the port number
	//     'e' is the time elapsed since last transition
	printLog("Train external transition \n");
}

Event train::lambda(double t) {
	//This function returns an Event:
	//     Event(%&Value%, %NroPort%) 
	//where:
	//     %&Value% points to the variable which contains the value.
	//     %NroPort% is the port number (from 0 to n-1)
	printLog("Train output \n");

	switch(s){
		case TRAIN_STATE_FAR:
			i = 0;
			y = TRAIN_APPROACH;
		break;
		case TRAIN_STATE_IN:
			i = 0;
			y = TRAIN_EXIT;
		break;
		case TRAIN_STATE_NEAR:
			i = 1;
			y = TRAIN_IN;
		break;
	}
	return Event(&y,i);
}

void train::exit() {
	printLog("Train exit \n");
	delete generator01;
	delete generatorKt1Kt2;
	delete generatorKt2;
}
