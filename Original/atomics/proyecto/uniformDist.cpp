#include "uniformDist.h"
#include <iostream>
#include <random>

//constructor con seed
uniformDist::uniformDist(double a, double b, unsigned sed){
	seed = sed;
	contador = 0;
	generator = new std::default_random_engine(seed);
	distribution = new std::uniform_real_distribution<double>(a,b);
} 

//constructor
uniformDist::uniformDist(double a, double b){
	std::random_device rd;
	seed = rd();
	contador = 0;
	generator = new std::default_random_engine(seed);
	distribution = new std::uniform_real_distribution<double>(a,b);
}

//Delete
uniformDist::~uniformDist(){
	delete generator;
	delete distribution;
}

//retorna un numero aleatorio entre a y b
double uniformDist::getNumber(){ 
	contador = contador+1;
 	return (*distribution)(*generator); 
}

//retorna la semilla
unsigned uniformDist::getSeed(){
	return seed;
}

//Cambia el minimo y el maximo de la funcion.
void uniformDist::setBounds(double min, double max){
	delete generator;
	delete distribution;
	generator = new std::default_random_engine(seed);
	distribution = new std::uniform_real_distribution<double>(min,max);

	//Pide n numeros para restaurar el estado anterior
	for (int i = 0; i < contador; ++i){
		this->getNumber();
		contador = contador-1;
	}
}