#include "mediator.h"

void mediator::init(double t,...) {
	//The 'parameters' variable contains the parameters transferred from the editor.
	printLog("Mediator init \n");
	va_list parameters;
	va_start(parameters,t);
	//To get a parameter: %Name% = va_arg(parameters,%Type%)
	//where:
	//      %Name% is the parameter name
	//	%Type% is the parameter type

	char *fvar= va_arg(parameters,char*);

	kc1=getScilabVar(fvar);
	fvar= va_arg(parameters,char*);
	kc2=getScilabVar(fvar);
	fvar= va_arg(parameters,char*);
	seedGenerator=getScilabVar(fvar);
	fvar= va_arg(parameters,char*);

	generator = new uniformDist(0.1,kc2,seedGenerator);
	s = MEDIATOR_STATE_SC1;
	sigma = INF;
	y = 0;
}

double mediator::ta(double t) {
//This function returns a double.
	printLog("Mediator Time advance \n");
	return sigma;
}

void mediator::dint(double t) {
	printLog("Mediator internal transition \n");
	if (s == MEDIATOR_STATE_SC4){
		s = MEDIATOR_STATE_SC1;
		sigma = INF;
	} else if (s == MEDIATOR_STATE_SC2){
		s = MEDIATOR_STATE_SC3;
		sigma = INF;
	}
}

void mediator::dext(Event x, double t) {
	//The input event is in the 'x' variable.
	//where:
	//     'x.value' is the value (pointer to void)
	//     'x.port' is the port number
	//     'e' is the time elapsed since last transition
	printLog("Mediator external transition \n");	
	double *xv = (double*)(x.value);
	if (s == MEDIATOR_STATE_SC1 && *xv==TRAIN_APPROACH){ //A=Approach
		s = MEDIATOR_STATE_SC2;
		sigma = kc1;
	}else if (s == MEDIATOR_STATE_SC3 && *xv==TRAIN_EXIT) { //E=Exit
			s = MEDIATOR_STATE_SC4;
			sigma = generator->getNumber(); //uniforme (0.1, kc2)
	}else{
			printLog("------------------------ERROR Mediator: caso no soportado\n");
	}
}

Event mediator::lambda(double t) {
	//This function returns an Event:
	//     Event(%&Value%, %NroPort%)
	//where:
	//     %&Value% points to the variable which contains the value.
	//     %NroPort% is the port number (from 0 to n-1)
	printLog("Mediator output \n");
	switch(s){
		case MEDIATOR_STATE_SC2:
			y = MEDIATOR_LOWER; //Lower
		break;
		case MEDIATOR_STATE_SC4:
			y = MEDIATOR_RAISE; //Raise
		break;
	}
	return Event(&y,0);
}

void mediator::exit() {
	//Code executed at the end of the simulation.
	printLog("Mediator exit \n");
	delete generator;
}