//CPP:proyecto/uniformDist.cpp
//CPP:proyecto/gate.cpp
#if !defined gate_h
#define gate_h

#include "simulator.h"
#include "event.h"
#include "stdarg.h"

#include "proyecto/uniformDist.h"
#include "proyecto/const.h"


class gate: public Simulator { 
// Declare the state,
double sigma;
int s;
int tempState;
double tempTime;

// output variables
double y;

// and parameters
double kg1;
double kg2;
double kg3;
unsigned int seedGenerator0Kg1;
unsigned int seedGeneratorKg2Kg3;

//generadores
uniformDist* generator0Kg1;
uniformDist* generatorKg2Kg3;



public:
	gate(const char *n): Simulator(n) {};
	void init(double, ...);
	double ta(double t);
	void dint(double);
	void dext(Event , double );
	Event lambda(double);
	void exit();
};
#endif
