//CPP:proyecto/uniformDist.cpp
#ifndef UNIFORMDIST_H
#define UNIFORMDIST_H

#include <iostream>
#include <random>

class uniformDist{

	private: 
		std::default_random_engine* generator;
		std::uniform_real_distribution<double>* distribution;
		unsigned seed;
		int contador;
	public: 
		uniformDist(double a, double b, unsigned seed); 
		uniformDist(double a, double b); 
		~uniformDist(); 
		double getNumber();
		unsigned getSeed();
		void setBounds(double min, double max);
};
#endif