Esta carpeta contiene 3 simulaciones cada una con 
sus archivos de registros de salidas. 

#REFERENCIAS DE GRAFICOS:
	TRAIN: 
		y = -1 -> state = Near
		y = 1 -> state = Far
		y = 0 -> state = In
	
	GATE:
		y = 1.2 -> state = Open
		y = 0.2 -> state = Close
		y = 0.7 -> state = Changing (Raising o Lowering) 
