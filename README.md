### Modelado y Simulación de Sistemas Dinámicos con el Formalismo DEVS
_D’alessandro Nicolás, García Valeria_

Se adjuntan:
* Informe
* Carpeta con 3 carpetas de las simulaciones nombradas en el informes (cada una contiene grafico, archivos de salida y semillas utilizadas para replicar la simulacion en caso de prueba).
* El modelo original
* El modelo con modificaciones
* Un programa en java `StatisticalInformation.java` que fue creado para realizar el analisis de salidas.


#### Para arreglar problemas con c++11
Editar en ~/powerdevs/engine/Makefile.include y agregarla flag que se encuentra entre asteriscos.

(Linux)
```Makefile
CXXFLAGS = **-std=c++11** -Wall $(OPTFLAGS)
```

(Windows)

```Makefile
CXXFLAGS = **-std=c++0x** -Wall $(OPTFLAGS)
```

### Camino Rapido
Para empezar a simular inmediatamente puede usar este "atajo".
Dependiendo de si quiere realizar la simulacion original o la que tiene modificaciones, puede usar estos comandos.

Para usar la simulacion sin modificaciones...
```bash
cd <path-to-repo>; cp -r Original/* ~/powerdevs
```
Para usar la simulacion con modificaciones ...
```bash
cd <path-to-repo>; cp -r Modificado/* ~/powerdevs
```
